import argparse

from crucilab.app import start_server


def parse_args(argv=None):
    parser = argparse.ArgumentParser(
        description="JIRA-Crucible-GitLab connector")
    parser.add_argument(
        "--env_file", "-e",
        help="A path to a .env file containing deployment configuration",
        default=None
    )

    return parser.parse_args(args=argv)


def main(argv=None):
    args = parse_args(argv)
    start_server(args.env_file)


if __name__ == "__main__":
    main()
