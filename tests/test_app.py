from mock import patch

from crucilab.app import on_review_start, start_server

from .files import get_path


@patch("crucilab.app._crucilab")
def test_on_review_start(crucilab):
    on_review_start("SOME_KEY")
    crucilab.create_merge_requests_for_issue.assert_called_once_with("SOME_KEY")


@patch("crucilab.app.JIRA")
@patch("crucilab.app.Gitlab")
@patch("crucilab.app.Crucilab")
@patch("crucilab.app.Crucible")
@patch("crucilab.app.app")
def test_start_server(app, crucible, crucilab, gitlab, jira):
    start_server(get_path("example.env"))
    app.run.assert_called_once_with(host="my.server.org", port=1234, debug=True)
    crucible.assert_called_once_with("http://crucible.com")
    gitlab.assert_called_once_with(
        "http://gitlab.com",
        private_token="test",
        api_version="4"
    )
    jira.assert_called_once_with(
        server="http://jira.com",
        basic_auth=("user", "123456")
    )
    crucilab.assert_called_once_with(
        app.logger, jira.return_value, gitlab.return_value,
        crucible.return_value)
