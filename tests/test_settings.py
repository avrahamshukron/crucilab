import mock
from os.path import join
from pytest import raises

from .files import AUX_DIR

from crucilab.settings import to_bool, Settings


def test_to_bool():
    assert not to_bool(False)
    assert not to_bool(0)
    assert not to_bool(0.0)
    assert not to_bool("False")
    assert not to_bool("false")

    assert to_bool(True)
    assert to_bool(1)
    assert to_bool(1.0)
    assert to_bool(5.0)
    assert to_bool("True")
    assert to_bool("true")

    with raises(ValueError):
        to_bool("somestring")
        to_bool(object())
        to_bool([])
        to_bool("")


def test_settings_from_env_file():
    Settings.load(join(AUX_DIR, "example.env"))
    assert Settings.DEBUG
    assert Settings.HOST == "my.server.org"
    assert Settings.PORT == 1234
    assert Settings.CRUCIBLE_SERVER == "http://crucible.com"
    assert Settings.JIRA_PASS == "123456"
    assert Settings.JIRA_USER == "user"
    assert Settings.JIRA_SERVER == "http://jira.com"
    assert Settings.GITLAB_PRIVATE_TOKEN == "test"
    assert Settings.GITLAB_SERVER == "http://gitlab.com"


@mock.patch("os.environ", {
    "DEBUG": "1",
    "HOST": "my.server.org",
    "PORT": "1234",
    "CRUCIBLE_SERVER": "http://crucible.com",
    "JIRA_PASS": "123456",
    "JIRA_USER": "user",
    "JIRA_SERVER": "http://jira.com",
    "GITLAB_PRIVATE_TOKEN": "test",
    "GITLAB_SERVER": "http://gitlab.com",
})
def test_load_without_file():
    Settings.load(env_file=None)
    assert Settings.DEBUG
    assert Settings.HOST == "my.server.org"
    assert Settings.PORT == 1234
    assert Settings.CRUCIBLE_SERVER == "http://crucible.com"
    assert Settings.JIRA_PASS == "123456"
    assert Settings.JIRA_USER == "user"
    assert Settings.JIRA_SERVER == "http://jira.com"
    assert Settings.GITLAB_PRIVATE_TOKEN == "test"
    assert Settings.GITLAB_SERVER == "http://gitlab.com"


@mock.patch("os.environ", {})
def test_missing_env():
    assert Settings.get_env("PORT", 1234) == 1234
    with raises(EnvironmentError):
        Settings.get_env("PORT")


@mock.patch("os.environ", {
    "DEBUG": 0,
    "HOST": "my.server.org",
    "PORT": "NaN",
    "CRUCIBLE_SERVER": "http://crucible.com",
    "JIRA_PASS": "123456",
    "JIRA_USER": "user",
    "JIRA_SERVER": "http://jira.com",
    "GITLAB_PRIVATE_TOKEN": "test",
    "GITLAB_SERVER": "http://gitlab.com",
})
def test_invalid_port():
    with raises(ValueError):
        Settings.apply_settings()
